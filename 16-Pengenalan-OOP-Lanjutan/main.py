# Pengenalan OOP - lanjutan

class Kalkulator:

    def __init__(self, nilai=0):
        self.nilai = nilai

    def tambah_angka(self, angka1, angka2):
        self.nilai = angka1+angka2
        if self.nilai > 9:
            print('kalkulator sederhana melebihi batas angka: {}'.format(self.nilai))
        return self.nilai


# Pewarisan
class KalkulatorKali(Kalkulator):

    def kali_angka(self, angka1, angka2):
        self.nilai = angka1*angka2
        return self.nilai

    # Menimpa metode kelas dasar
    def tambah_angka(self, angka1, angka2):
        self.nilai = angka1+angka2
        return self.nilai


class KalkulatorTambah(Kalkulator):

    # Menimpa metode kelas dasar
    def tambah_angka(self, angka1, angka2):
        if angka1+angka2 <= 9:
            # Memanggil metode kelas dasar
            super().tambah_angka(angka1, angka2)
        else:
            self.nilai = angka1+angka2
        return self.nilai


class Pegawai:
    pass


kk = KalkulatorKali()
a = kk.kali_angka(2, 3)
print(a)

b = kk.tambah_angka(5, 6)
print(b)

kt = KalkulatorTambah()
c = kt.tambah_angka(5, 6)
print(c)

# Membuat sebuah struktur data
# bertujuan menyatukan sejumlah
# penamaan item data menjadi satu.
don = Pegawai()
don.nama = 'Don Doo'
don.bagian = 'IT'
don.gaji = 999