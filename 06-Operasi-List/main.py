# Operasi pada list dan manipulasi string

# Menghitung panjang atau banyaknya elemen dari List
# (untuk String menjadi menghitung jumlah karakternya).
l = [1, 2, 3, 3, 4, 4, 4, 4, 5, 6]
s = {1, 2, 3, 4, 5, 6}
x = "Hello, World!"

print(l)
print(len(l))

print(s)
print(len(s))

print(x)
print(len(x))

print('-'*64)

# Penggabungan dan Replikasi
print([1, 2, 3] + ['A', 'B', 'C'])

print(['X', 'Y', 'Z'] * 3)

spam = [1, 2, 3]
spam = spam + ['A', 'B', 'C']
print(spam)

arr = [0]*10
len(arr)
print(arr)

print('-'*64)

# Range
for i in range(9):
    print(i, end=" ")

print()

for i in range(3, 9):
    print(i, end=" ")

print()

print([_ for _ in range(1, 9, 2)])  # list comprehension

print('-'*64)

# in and not in
print('howdy' in ['hello', 'hi', 'howdy', 'heyas'])

spam = ['hello', 'hi', 'howdy', 'heyas']
print('cat' in spam)
print('howdy' not in spam)
print('cat' not in spam)

print('-'*64)

# Memberikan nilai untuk lebih dari 1 variabel sekaligus.
cat = ['fat', 'orange', 'loud']  # from list
size, color, disposition = cat
cat = ('fat', 'orange', 'loud')  # from tuple
size, color, disposition = cat

a, b = 'Alice', 'Bob'
a, b = b, a
print(a)
print(b)

print('-'*64)

# Mengurutkan list
x = [2, 5, 3.14, 1, -7]
x.sort()
print(x)

y = ['ants', 'cats', 'dogs', 'badgers', 'elephants']
y.sort()
print(y)
y.sort(reverse=True)
print(y)

m = ['Alice', 'ants', 'Bob', 'badgers', 'Carol', 'cats']
m.sort()
print(m)

spam = ['a', 'z', 'A', 'Z']
spam.sort(key=str.lower)
print(spam)

print('-'*64)

# Manipulasi string
st = "That is Alice's cat"
print(st)
st = 'Say hi to Bob\'s Mother'
print(st)
print("Hello there!\nHow are you?\nI'm doing fine.")

multi_line = """Hello there!
How are you?
I'm fine."""
print(multi_line)

# Raw String
print(r'That is Carol\'s cat.')