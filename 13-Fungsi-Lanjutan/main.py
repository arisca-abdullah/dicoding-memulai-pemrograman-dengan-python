# Fungsi - lanjutan

# Argument dengan panjang yang dinamis.
def printinfo(fixedarg, *args):
    print('Output: fixedarg {}'.format(fixedarg))
    for a in args:
        print('Argumen posisi {}'.format(a))

printinfo(10)
printinfo(70, 60, 50)

def printinfo2(*args, **kwargs):
    for a in args:
        print('Argumen posisi {}'.format(a))
    for key, value in kwargs.items():
        print('Argumen kata kunci {}:{}'.format(key, value))

printinfo2()
printinfo2(1, 2, 3)
printinfo2(i=7, j=8, k=9)
printinfo2(1, 2, j=8, k=9)
printinfo2(*(2, 3), **{'i': 8, 'j': 9})

# lambda function
sum = lambda arg1, arg2: arg1 + arg2
print('Value of total:', sum(10, 20))
print('Value of total:', sum(20, 20))

# Cakupan variabel
total = 0  # variabel global
def sum2(arg1, arg2):
    total = arg1 + arg2  # variabel lokal
    print('Inside the function: {}'.format(total))
sum2(10, 20)
print('Outside the function: {}'.format(total))

# Menggunakan variabel global dalam fungsi
def sum3(arg1, arg2):
    global total
    total = arg1 + arg2
    print('Inside the function: {}'.format(total))
sum3(10, 20)
print('Outside the function: {}'.format(total))