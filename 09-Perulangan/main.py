# Perulangan

# for
for letter in 'Python':
    print('Current letter: {}'.format(letter))
fruits = ['banana', 'apple', 'mango']
for fruit in fruits:
    print('Current fruit: {}'.format(fruit))
for fruit in range(len(fruits)):
    print('Current fruit: {}'.format(fruits[fruit]))

# while
count = 0
while count < 5:
    print('The count is {}'.format(count))
    count += 1

loop = True
while loop:
    input_ = input('Ulangi lagi? (y/n) ')
    loop = True if input_ == 'y' else False

# Perulangan bertingkat
for i in range(5):
    for j in range(5-i):
        print('*', end='')
    print()