# Menambahkan awalan 0 pada angka
x = 1
print(str(x).zfill(5))

print('-'*64)

# Membuat seluruh karakter dalam string
# menjadi kapital atau huruf kecil.
p = "Hello, World!"
print(p.upper())
print(p.lower())

print('-'*64)

# Memeriksa isi dari string.
print(p.isupper())
print(p.islower())
print(p.lower().islower())

print(p.isalpha())
print(p.isalnum())
print(p.isdecimal())
print(p.isspace())
print(p.istitle())

print('-'*64)

# Mengecek nilai awalan atau akhiran string
print(p.startswith("Hello"))
print(p.endswith("World"))

print('-'*64)

# Menggabungkan string
print(', '.join(['cats', 'rats', 'bats']))
print(' '.join(['My', 'name', 'is', 'John']))

print('-'*64)

# Memisahkan string
print("My name is John".split())
print("cats, rats, bats".split(', '))

a = '''Dear Alice,
How have you been? I am fine.
There is a container in the fridge
that is labeled "Milk Experiment".
Please do not drink it.
Sincerely,
Bob'''
print(a.split('\n'))

print('-'*64)

# Merapikan percetakan teks
print("Hello".rjust(20))
print("Hello".rjust(20, '*'))
print("Hello".ljust(20, '-'))
print("Hello".center(20, '='))

print('-'*64)

# Menghapus whitespace
spam = "     Hello, World!     "
print(spam.strip())
print(spam.lstrip())
print(spam.rstrip())
spam = "SpamSpamBaconSpamEggsSpamSpam"
print(spam.strip('ampS'))

print('-'*64)

# Mengganti string
string = "geeks for geeks geeks geeks geeks"
print(string.replace("geeks", "Geeks"))
print(string.replace("geeks", "GeeksforGeeks", 3))