# Dynamic Typing
#   Tipe variabel dapat berubah saat program dijalankan

x = 6
print(type(x))
x = 'hello'
print(type(x))