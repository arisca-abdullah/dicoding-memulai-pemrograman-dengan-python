# Percabangan

# if
var1 = 100
if var1:
    print("1 - Got a true expression value")
    print(var1)
var2 = 0
if var2:
    print("2 - Got a true expression value")
    print(var2)
var3 = 50
if var3: print(var3)  # menyingkat penulisan

print('-'*64)

# else
amount = int(input("Enter amount: "))
if(amount<1000):
    discount = amount*5/100
    print("Discount", discount)
else:
    discount = amount*10/100
    print("Discount", discount)
print("Net payable:", amount-discount)

print('-'*64)

a = 8
if a % 2 == 0:
    print("Bilangan {} adalah bilangan genap.".format(a))
else:
    print("Bilangan {} adalah bilangan ganjil.".format(a))

print('-'*64)

# elif
amount = int(input("Enter amount: "))
if amount<1000:
    discount = amount*5/100
    print("Discount", discount)
elif amount<5000:
    discount = amount*10/100
    print("Discount", discount)
else:
    discount = amount*15/100
    print("Discount", discount)
print("Net payable:", amount-discount)

a = 0
if a > 0:
    print("Bilangan {} adalah bilangan positif.".format(a))
elif a < 0:
    print("Bilangan {} adalah bilangan negatif.".format(a))
else:
    print("Bilangan {} adalah nol".format(a))

print('-'*64)

# Ternary operator
is_nice = True
state = "nice" if is_nice else "not nice"
print(state)

nice = True
personality = ("mean", 'nice')[nice]
print(personality)

# ShortHand ternary
output = None
msg = output or "No data returned"
print(msg)