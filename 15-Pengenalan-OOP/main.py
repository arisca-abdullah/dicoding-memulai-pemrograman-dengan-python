# Pengenalan OOP

# mendefinisikan kelas
class Kalkulator:

    # membuat constructor
    def __init__(self, i=12345):
        self.i = i

    # membuat method
    def f(self):
        return 'Hello, World!'

    @classmethod
    def tambah_angka(cls, angka1, angka2):
        return '{} + {} = {}'.format(angka1, angka2, angka1+angka2)

    @staticmethod
    def kali_angka(angka1, angka2):
        return '{} x {} = {}'.format(angka1, angka2, angka1*angka2)


k = Kalkulator()
print(k.i)  # mengakses atribut

k2 = Kalkulator(1000)
print(k2.i)

# Menggunakan class method
print(Kalkulator.tambah_angka(1, 2))
print(k2.tambah_angka(1, 2))

# Menggunakan static method
print(Kalkulator.kali_angka(2, 3))
print(k2.kali_angka(2, 3))