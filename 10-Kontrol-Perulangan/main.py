# Kontrol Perulangan

import sys

# break
for letter in 'Python':
    if letter == 'h':
        break
    print('Current letter: {}'.format(letter))

var = 10
while var > 0:
    print("Current variable value: {}".format(var))
    var -= 1
    if var == 5:
        break

# continue
for letter in 'Python':
    if letter == 'h':
        continue
    print('Current letter: {}'.format(letter))

for a in [0, 1, -1, 2, -2, 3, -3]:
    if a <= 0:
        continue
    print('Bilangan positif: {}'.format(a))

var = 10
while var > 0:
    var -= 1
    if var == 5:
        continue
    print('Current variable value: {}'.format(var))

# for else
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, 'equals', x, '*', n//x)
    else:
        print(n, 'is prime number')

# while else
n = 5
while n > 0:
    n -= 1
    if n == 2:
        break
    print(n)
else:
    print('Loop is finished')

# pass
for letter in 'Python':
    if letter.isupper():
        pass
    else:
        print('Lower letter: {}'.format(letter))

data=''
while data != 'exit':
    try:
        data = input('Please enter an integer (type exit to exit): ')
        print('Got integer value: {}'.format(int(data)))
    except:
        if data == 'exit':
            pass
        else:
            print('Error: {}'.format(sys.exc_info()[0]))

# List comprehension
numbers = [1, 2, 3, 4]
squares = [n**2 for n in numbers]
print(squares)

list_a = [1, 2, 3, 4]
list_b = [2, 3, 4, 5]
commom_num = [a for a in list_a for b in list_b if a == b]
print(commom_num)

list_a = ['Hello', 'World', 'In', 'Python']
small_list = [_.lower() for _ in list_a]
print(small_list)

list_a = range(1, 10, 2)
x = [[a**2, a**3] for a in list_a]
print(x)