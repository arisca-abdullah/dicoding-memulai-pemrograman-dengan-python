# Input dan Output

# Output
print("Hello, World!")

a = 5
print("The value of a is", a)

name = "John"
age = 15
print('Hello, {}!'.format(name))
print("%s is %d years old." % (name, age))

# Contoh menambahkan objek selain string (otomatis dikonversi):
mylist = [1, 2, 3]
print("A list: %s" % mylist)

a, b = 10, 11
print('a: %x and b: %X' % (a, b))

print('-'*64)

# Input
num = input('Enter a number: ')
print(num)
print(int(num))