# Penanganan Pengecualian

z = 0
try:
    x = 1/z
    print(x)
except ZeroDivisionError:
    print("Tak terdefinisi (~)")

try:
    with open('contoh_tidak_ada.py') as file:
        print(file.read())
except (FileNotFoundError, ):
    print("File tidak ditemukan")

d = {'ratarata': '10.0'}
try:
    print('Rata-rata: {}'.format(d['rata-rata']))
except KeyError:
    print('Kunci tidak ditemukan pada dictionary')
except ValueError:
    print('Nilai tidak sesuai')

try:
    print('Rata-rata: {}'.format(d['ratarata']/3))
except KeyError:
    print('Kunci tidak ditemukan pada dictionary')
except (TypeError, ValueError):
    print('Nilai atau tipe tidak sesuai')

try:
    print('Pembulatan rata-rata: {}'.format(int(d['ratarata'])))
except (ValueError, TypeError) as e:
    print('Kesalahan: {}'.format(e))

# Menghasilkan pengecualian
d = {'rata-rata': '10.0', 'total': '10.0'}
if 'total' not in d:
    # Menghasilkan pengecualian jika dictionary d tidak
    # memiliki total 
    raise KeyError('Dictionary d harus memiliki total')