# Tipe Data (2)

# Tuple
#   Jenis dari list yang tidak dapat diubah elemennya.
t = (5, 'program', 1+3j)
print(t)
print(t[1])
print(t[0:3])

print('-'*64)

# Set
#   Kumpulan item bersifat unik dan tanpa urutan (unordered collection).
#   Pada Set kita dapat melakukan union dan intersection,
#   sekaligus otomatis melakukan penghapusan data duplikat.
a = {1, 2, 2, 3, 3}
print(a)

print('-'*64)

# Dictionary
#   Kumpulan pasangan kunci-nilai (pair of key-value)
#   yang bersifat tidak berurutan.
d = {'key': 1, 2: 'value'}
print(d)
print(type(d))
print(d['key'])
print(d[2])

print('-'*64)

# Konversi tipe data

# Konversi int ke float.
print(float(5))

# Konversi float ke int akan bersifat floor/truncating,
# menghilangkan nilai di belakang koma.
print(int(10.6))

# Konversi dari-dan-ke string akan melalui pengujian
# dan dipastikan validitasnya.
print(float('2.5'))
print(str(25))

# Konversi kumpulan data (set, list, tuple).
print(set([1, 2, 3]))
print(tuple({5, 6, 7}))
print(list('hello'))

# Untuk konversi ke dictionary, data harus memenuhi
# persyaratan key-value.
print(dict([[1, 2], [3, 4]]))
print(dict([(3, 26), (4, 44)]))