# Tipe Data

# Numbers
#   Tipe numerik pada Python dibagi menjadi 3: int, float, complex.
#   Integer tidak dibatasi oleh angka atau panjang tertentu,
#   namun dibatasi oleh memori yang tersedia.
#   Bilangan pecahan dibatasi akurasinya pada 15 desimal.
#   Python juga mendukung bilangan imajiner dan bilangan kompleks.
a = 5
print(a, "is type of", type(a))
a = 2.0
print(a, "is type of", type(a))
a = 1+2j
print(a, "is complex number?", isinstance(a, complex))

print('-'*64)

# Strings
#   Urutan dari karakter unicode.
s = "This is a string"
print(s)
s = '''This is multiline string
a new line
another new line'''
print(s)
s = "Hello, World!"
print(s)
print(s[4])
print(s[7:12])

print('-'*64)

# Boolean
#   Turunan dari bilangan bulat (integer atau int)
#   yang hanya punya dua nilai konstanta: True dan False.

# List
#   Jenis kumpulan data terurut (ordered sequence).
x = [5, 10, 15, 20, 25, 30, 35, 40]
print(x[5])
print(x[-1])
print(x[3:5])
print(x[:5])
print(x[-3:])
print(x[1:7:2])

#   Mengubah elemen pada list
x = [1, 2, 3]
print(x)
x[2] = 4
print(x)

#   Menambah elemen pada list
x.append(5)
print(x)

# Menghapus elemen pada list
spam = ['cat', 'bat', 'rat', 'elephant']
print(spam)
del spam[2]
print(spam)