# Fungsi

# membuat fungsi
def print_me(str):
    print(str)

# memanggil fungsi
print_me("Hello, World!")

def sum(arg1, arg2):
    """
    Fungsi ini digunakan untuk menjumlahkan
    dua parameter
    """
    total = arg1 + arg2
    print('Inside the function: {}'.format(total))
    return total

total = sum(10, 20)
print('Outside the function: {}'.format(total))

def changeme(mylist):
    mylist.append([1, 2, 3, 4])
    print('Nilai di dalam fungsi: {}'.format(mylist))

mylist = [10, 20, 30]
changeme(mylist)
print('Nilai di luar fungsi: {}'.format(mylist))

def changeme2(mylist):
    mylist = [1, 2, 3, 4]
    print('Nilai di dalam fungsi: {}'.format(mylist))

mylist = [10, 20, 30]
changeme2(mylist)
print('Nilai di luar fungsi: {}'.format(mylist))

def printinfo(name, age):
    print('Name:', name)
    print('Age:', age)

printinfo(age=17, name='Aris')

def printinfo2(name, age=20):
    print('Name:', name)
    print('Age:', age)

printinfo2('Aris', 17)
printinfo2('Asep')