# Operator and ekspresi
import operator

# Operator aritmatika
print(3+5)  # penjumlahan angka
print('a'+'b')  # penjumlahan string
print(50-24)  # pengurangan
print(2*3)  # perkalian angka
print('la'*3)  # perkalian string
print(3**4)  # perpangkatan
print(13/3)  # pembagian
print(13//3)  # pembagian dan dibulatkan
print(9//1.81)  # pembagian dan dibulatkan
print(13%3)  # sisa bagi
print(-25.5 % 2.25)  # sisa bagi

print('-'*64)

# Operasi bit

# Menggeser representasi bit/binary dari operand
# pertama sebanyak operand kedua ke kiri.
print(2<<2)

# Menggeser representasi bit/binary dari operand
# pertama sebanyak operand kedua ke kanan.
print(11>>1)

# Menjalankan operasi binary AND pada
# representasi operand pertama dan kedua.
print(5&3)

# Menjalankan operasi binary OR pada
# representasi operand pertama dan kedua.
print(5|3)

# Menjalankan operasi binary XOR pada
# representasi operand pertama dan kedua.
print(5^3)

# Menjalankan operasi binary invert pada
# representasi operand.
print(~5)

print('-'*64)

# Operator Perbandingan
print(5<3)  # kurang dari
print(5>3)  # lebih dari

x = 3; y = 6
print(x<=y)  # kurang dari sama dengan

x = 4; y = 3
print(x>=y)  # lebih dari sama dengan

x = 2; y = 2
print(x==y)  # sama dengan

x = 'str'; y = 'stR'
print(x==y)  # sama dengan

x = 'str'; y = 'str'
print(x==y)  # sama dengan

x = 2; y = 3
print(x!=y)  # tidak sama dengan

print('-'*64)

# lt, le, gt, ge, eq, ne
a = 1
b = 5.0

print(operator.lt(a, b))  # less than
print(operator.le(a, b))  # less than or equal to
print(operator.gt(a, b))  # greater than
print(operator.ge(a, b))  # greater than or equal to
print(operator.eq(a, b))  # equal to
print(operator.ne(a, b))  # not equal to

print('-'*64)

# Boolean operator

x = True
print(not x)  # boolean not

x = False
y = True
print(x and y)  # boolean and

x = True
Y = False
print(x or y)  # boolean or

print('-'*64)

# Menyingkat penulisan operasi
a = 2
a *= 3  # a = a * 3
print(a)